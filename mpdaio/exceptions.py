# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2012-2024  kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

class MPDError(Exception):
    """Main musicpd Exception"""


class MPDConnectionError(MPDError):
    """Fatal Connection Error, cannot recover from it."""


class MPDProtocolError(MPDError):
    """Fatal Protocol Error, cannot recover from it"""

class MPDCommandError(MPDError):
    """Malformed command, socket should be fine, can reuse it"""
