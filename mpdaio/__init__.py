# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2012-2024  kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

from .client import MPDClient
