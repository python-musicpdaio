# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2012-2024  kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

HELLO_PREFIX = 'OK MPD '
ERROR_PREFIX = 'ACK '
SUCCESS = 'OK'
NEXT = 'list_OK'
VERSION = '0.1.0b0'
#: Seconds before a connection attempt times out
#: (overriden by :envvar:`MPD_TIMEOUT` env. var.)
CONNECTION_TIMEOUT = 30
#: Socket timeout in second > 0 (Default is :py:obj:`None` for no timeout)
SOCKET_TIMEOUT = None
#: Maximum concurrent connections
CONNECTION_MAX = 100
