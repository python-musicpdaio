#!/usr/bin/env python3
# coding: utf-8
# SPDX-FileCopyrightText: 2012-2024  kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: LGPL-3.0-or-later
# pylint: disable=missing-docstring

import unittest
import unittest.mock
import os
import warnings

import mpdaio.client
import mpdaio.const

# show deprecation warnings
warnings.simplefilter('default')


class TestEnvVar(unittest.TestCase):

    def test_envvar(self):
        # mock "os.path.exists" here to ensure there are no socket in
        # XDG_RUNTIME_DIR/mpd or /run/mpd since with test defaults fallbacks
        # when :
        #   * neither MPD_HOST nor XDG_RUNTIME_DIR are not set
        #   * /run/mpd does not expose a socket
        with unittest.mock.patch('os.path.exists', return_value=False):
            os.environ.pop('MPD_HOST', None)
            os.environ.pop('MPD_PORT', None)
            client = mpdaio.client.MPDClient()
            self.assertEqual(client.host, 'localhost')
            self.assertEqual(client.port, '6600')

            os.environ.pop('MPD_HOST', None)
            os.environ['MPD_PORT'] = '6666'
            client = mpdaio.client.MPDClient()
            self.assertEqual(client.pwd, None)
            self.assertEqual(client.host, 'localhost')
            self.assertEqual(client.port, '6666')

        # Test password extraction
        os.environ['MPD_HOST'] = 'pa55w04d@example.org'
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.pwd, 'pa55w04d')
        self.assertEqual(client.host, 'example.org')

        # Test host alone
        os.environ['MPD_HOST'] = 'example.org'
        client = mpdaio.client.MPDClient()
        self.assertFalse(client.pwd)
        self.assertEqual(client.host, 'example.org')

        # Test password extraction (no host)
        os.environ['MPD_HOST'] = 'pa55w04d@'
        with unittest.mock.patch('os.path.exists', return_value=False):
            client = mpdaio.client.MPDClient()
        self.assertEqual(client.pwd, 'pa55w04d')
        self.assertEqual(client.host, 'localhost')

        # Test badly formatted MPD_HOST
        os.environ['MPD_HOST'] = '@'
        with unittest.mock.patch('os.path.exists', return_value=False):
            client = mpdaio.client.MPDClient()
        self.assertEqual(client.pwd, None)
        self.assertEqual(client.host, 'localhost')

        # Test unix socket extraction
        os.environ['MPD_HOST'] = 'pa55w04d@/unix/sock'
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.host, '/unix/sock')

        # Test plain abstract socket extraction
        os.environ['MPD_HOST'] = '@abstract'
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.host, '@abstract')

        # Test password and abstract socket extraction
        os.environ['MPD_HOST'] = 'pass@@abstract'
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.pwd, 'pass')
        self.assertEqual(client.host, '@abstract')

        # Test unix socket fallback
        os.environ.pop('MPD_HOST', None)
        os.environ.pop('MPD_PORT', None)
        os.environ.pop('XDG_RUNTIME_DIR', None)
        with unittest.mock.patch('os.path.exists', return_value=True):
            client = mpdaio.client.MPDClient()
            self.assertEqual(client.host, '/run/mpd/socket')
            os.environ['XDG_RUNTIME_DIR'] = '/run/user/1000'
            client = mpdaio.client.MPDClient()
            self.assertEqual(client.host, '/run/user/1000/mpd/socket')

        os.environ.pop('MPD_HOST', None)
        os.environ.pop('MPD_PORT', None)
        os.environ['XDG_RUNTIME_DIR'] = '/run/user/1000/'
        with unittest.mock.patch('os.path.exists', return_value=True):
            client = mpdaio.client.MPDClient()
            self.assertEqual(client.host, '/run/user/1000/mpd/socket')

        # Test MPD_TIMEOUT
        os.environ.pop('MPD_TIMEOUT', None)
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.mpd_timeout, mpdaio.const.CONNECTION_TIMEOUT)
        os.environ['MPD_TIMEOUT'] = 'garbage'
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.mpd_timeout,
                         mpdaio.const.CONNECTION_TIMEOUT,
                         'Garbage is silently ignore to use default value')
        os.environ['MPD_TIMEOUT'] = '42'
        del client
        client = mpdaio.client.MPDClient()
        self.assertEqual(client.mpd_timeout, 42)
