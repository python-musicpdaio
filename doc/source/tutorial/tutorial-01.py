import asyncio
import logging

from mpdaio import MPDClient

# Configure loggers
logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s')
logging.getLogger("asyncio").setLevel(logging.WARNING)
# debug level level will show where defaults settings come from
log = logging.getLogger('mpdaio.client')
log.setLevel(logging.DEBUG)


async def search(fltr):
    # Look for and add
    await client.searchadd(fltr)


async def run():

    # Make an initial connection to MPD server
    # The connection is kept open an reused for later commands
    await client.ping()
    await client.clear()
    filters = [
            '(Artist == "Neurosis")',
            '(Artist == "Isis")',
            '(Artist == "Cult of Luna")',
            ]
    # Each task gathered here will run with it's own connection
    await asyncio.gather(*map(search, filters))

    # Closes all connections to MPD server
    await client.close()


if __name__ == '__main__':
    # Use defaults to access MPD server
    client = MPDClient()
    asyncio.run(run())
