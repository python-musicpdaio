# mpd-client.py
import asyncio
import logging

from mpdaio import MPDClient

# Configure loggers
logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s')
logging.getLogger("asyncio").setLevel(logging.WARNING)
# debug level level will show where defaults settings come from
log = logging.getLogger('mpdaio.client')
log.setLevel(logging.DEBUG)


async def run():
    # Explicit host declaration
    #client = MPDClient(host='example.org', port='6601')

    # Use defaults
    client = MPDClient()
    # MPDClient use MPD_HOST/MPD_PORT env var if set
    # else test ${XDG_RUNTIME_DIR}/mpd/socket for existence
    # finnally fallback to localhost:6600

    # Make an initial connection to MPD server
    # The connection is kept open an reused for later commands
    await client.ping()

    # Get player status
    status = await client.status()
    if status.get('state') == 'play':
        current_song_id = status.get('songid')
        current_song = await client.playlistid(current_song_id)
        log.info(f'Playing   : {current_song[0].get("file")}')
        next_song_id = status.get('nextsongid', None)
        if next_song_id:
            next_song = await client.playlistid(next_song_id)
            log.info(f'Next song : {next_song[0].get("file")}')
    else:
        log.info('Not playing')

    # Add all songs form artist "The Doors"
    await client.searchadd('(Artist == "The Doors")')
    # Start playing
    if (await client.status()).get('state') != 'play':
        await client.play()


    # Closes any remaining connections to MPD server
    await client.close()


if __name__ == '__main__':
    asyncio.run(run())
