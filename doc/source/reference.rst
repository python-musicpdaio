.. _reference:

Reference
=========

.. _environment_variables:

Environment variables
---------------------

:py:class:`mpdaio.MPDClient<mpdaio.client.MPDClient>` honors the following environment variables:

.. envvar:: MPD_HOST

   MPD host (:abbr:`FQDN (fully qualified domain name)`, IP, socket path or abstract socket) and password.

    | To define a **password** set :envvar:`MPD_HOST` to "*password@host*" (password only "*password@*")
    | For **abstract socket** use "@" as prefix : "*@socket*" and then with a password  "*pass@@socket*"
    | Regular **unix socket** are set with an absolute path: "*/run/mpd/socket*"

.. envvar:: MPD_PORT

   MPD port, relevant for TCP socket only

.. envvar:: MPD_TIMEOUT

   socket timeout when connecting to MPD and waiting for MPD’s response (in seconds)

.. envvar:: XDG_RUNTIME_DIR

   path to look for potential socket

.. _default_settings:

Default settings
----------------

Default host:
 * use :envvar:`MPD_HOST` environment variable if set, extract password if present,
 * else looks for an existing file in :envvar:`${XDG_RUNTIME_DIR:-/run/}/mpd/socket`
 * else set host to ``localhost``

Default port:
 * use :envvar:`MPD_PORT` environment variable if set
 * else use ``6600``

Default timeout:
 * use :envvar:`MPD_TIMEOUT` if set
 * else use :py:obj:`mpdaio.const.CONNECTION_TIMEOUT`

Supported commands
------------------

.. include:: commands.rst

Module documentation
--------------------

MPDClient class
^^^^^^^^^^^^^^^

.. automodule:: mpdaio.client
   :members:

Constants
^^^^^^^^^

.. automodule:: mpdaio.const
   :members:

.. vim: spell spelllang=en
