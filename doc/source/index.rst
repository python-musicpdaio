musicpdaio's documentation
==========================

The documentation is structured by following the `Diátaxis`_ principles:
tutorials and explanation are mainly useful to discover and learn, howtos and
reference are more useful when you are familiar with musicpdaio already and you
have some specific action to perform or goal to achieve.

.. note::

   This module is still young, its API probably not stable.

.. toctree::
   :maxdepth: 2

   tutorial

   explanations

   reference

   changelog

   development

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. meta::
   :description: musicpdaio, an asynchronous MPD (Music Player Daemon) client library written in Python.
   :keywords: Sphinx, documentation, musicpdaio, python-musicpdaio, MPD, python, Music Player Daemon
