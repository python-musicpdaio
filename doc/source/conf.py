# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import pathlib
import sys

sys.path.insert(0, pathlib.Path(__file__).parents[2].resolve().as_posix())
from mpdaio.const import VERSION

project = 'musicpdaio'
copyright = '2024, kaliko'
author = 'kaliko'

version = VERSION
release = VERSION

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
]

templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
html_baseurl= 'https://musicplayerdaemon.codeberg.page/python-musicpdaio/'
html_theme_options = {
    'page_width' : '1024px',
    'fixed_sidebar': 'true',
    'base_bg': '#eee',
    'pre_bg': '#fff',
    'note_bg': '#fff',
    }

rst_epilog = """
.. _asyncio: https://docs.python.org/3/library/asyncio.html
.. _Diátaxis: https://diataxis.fr/
.. _codeberg forge: https://codeberg.org/MusicPlayerDaemon/python-musicpdaio
.. _MPD protocol documentation: http://www.musicpd.org/doc/protocol/
.. _Music Player Daemon: http://www.musicpd.org/
.. _python-musicpd: https://kaliko.gitlab.io/python-musicpd
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _snake case: https://en.wikipedia.org/wiki/Snake_case
.. |mpdaio.MPDClient| replace:: :py:class:`mpdaio.MPDClient<mpdaio.client.MPDClient>`
"""

autodoc_typehints = 'description'
autodoc_member_order = 'bysource'

# -- Options for intersphinx extension ---------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#configuration

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
}
