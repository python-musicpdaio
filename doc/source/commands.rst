
Status Commands
^^^^^^^^^^^^^^^
* clearerror
* currentsong
* idle
* noidle
* status
* stats

Playback Option Commands
^^^^^^^^^^^^^^^^^^^^^^^^
* consume
* crossfade
* mixrampdb
* mixrampdelay
* random
* repeat
* setvol
* getvol
* single
* replay_gain_mode
* replay_gain_status
* volume

Playback Control Commands
^^^^^^^^^^^^^^^^^^^^^^^^^
* next
* pause
* play
* playid
* previous
* seek
* seekid
* seekcur
* stop

Queue Commands
^^^^^^^^^^^^^^
* add
* addid
* clear
* delete
* deleteid
* move
* moveid
* playlist
* playlistfind
* playlistid
* playlistinfo
* playlistsearch
* plchanges
* plchangesposid
* prio
* prioid
* rangeid
* shuffle
* swap
* swapid
* addtagid
* cleartagid

Stored Playlist Commands
^^^^^^^^^^^^^^^^^^^^^^^^
* listplaylist
* listplaylistinfo
* listplaylists
* load
* playlistadd
* playlistclear
* playlistdelete
* playlistlength
* playlistmove
* rename
* rm
* save

Database Commands
^^^^^^^^^^^^^^^^^
* albumart
* count
* getfingerprint
* find
* findadd
* list
* listall
* listallinfo
* listfiles
* lsinfo
* readcomments
* readpicture
* search
* searchadd
* searchaddpl
* searchcount
* update
* rescan

Mounts and neighbors
^^^^^^^^^^^^^^^^^^^^
* mount
* unmount
* listmounts
* listneighbors

Sticker Commands
^^^^^^^^^^^^^^^^
* sticker get
* sticker set
* sticker delete
* sticker list
* sticker find
* stickernames

Connection Commands
^^^^^^^^^^^^^^^^^^^
* close
* kill
* password
* ping
* binarylimit
* tagtypes
* tagtypes disable
* tagtypes enable
* tagtypes clear
* tagtypes all

Partition Commands
^^^^^^^^^^^^^^^^^^
* partition
* listpartitions
* newpartition
* delpartition
* moveoutput

Audio Output Commands
^^^^^^^^^^^^^^^^^^^^^
* disableoutput
* enableoutput
* toggleoutput
* outputs
* outputset

Reflection Commands
^^^^^^^^^^^^^^^^^^^
* config
* commands
* notcommands
* urlhandlers
* decoders

Client to Client
^^^^^^^^^^^^^^^^
* subscribe
* unsubscribe
* channels
* readmessages
* sendmessage
