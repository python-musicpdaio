.. _explanations:

Explanations
============

 |  « Concurrency is about dealing with lots of things at once. »
 |  Concurrency is not Parallelism -- Rob Pike


What is musicpdaio?
-------------------

**musicpdaio** is an asynchronous MPD (`Music Player Daemon`_) client library written in Python. It is the asyncio_ port of python-musicpd_.

The goal of this project is to keep python-musicpd_ simplicity and provide
asyncio_ support.

Should I use it?
----------------

 * If you need a plain MPD client to manage you MPD server, then stick with
   non-asyncio module python-musicpd_
 * If you're building an interactive client, concurrent access to MPD or need
   to plug into another asyncio project then use musicpdaio.


Using the client library
-------------------------

The client library can be used as follows:

.. code-block:: python

    client = mpdaio.MPDClient()       # create client object
    await client.ping()               # test the connection, using default host/port, cf. Reference
    print(client.version)             # print the MPD protocol version
    await client.setvol('42')         # sets the volume
    await client.close()              # disconnect from the server

The MPD command protocol exchanges line-based text records. The client emits a
command with optional arguments. In the example above the client sends a
`setvol` command with the string argument `42`.

MPD commands are exposed as |mpdaio.MPDClient| methods. Methods
**arguments are python strings**. Some commands are composed of more than one word
(ie "**tagtypes [disable|enable|all]**"), for these use a `snake case`_ style to
access the method. Then **"tagtypes enable"** command is called with
**"tagtypes_enable"**.

Remember MPD protocol is text based, then all MPD command arguments are UTF-8
strings. In the example above, an integer can be used as argument for the
`setvol` command, but it is then evaluated as a string when the command is
written to the socket. To avoid confusion use regular string instead of relying
on object string representation.

|mpdaio.MPDClient| methods returns different kinds of objects
depending on the command. Could be :py:obj:`None`, a single object as a
:py:obj:`str` or a :py:obj:`dict`, a list of :py:obj:`dict`.

Then |mpdaio.MPDClient| **methods signatures** are not hard coded
within this module since the protocol is handled on the server side. Please
refer to the protocol and MPD commands in `MPD protocol documentation`_ to
learn how to call commands and what kind of arguments they expect.

Some examples are provided for the most common cases, see :ref:`tutorial`.

**musicpdaio** tries to come with sane defaults, then running
|mpdaio.MPDClient| with no explicit argument will try default values
to connect to MPD. Cf. :ref:`reference` for more about
:ref:`defaults<default_settings>`.

.. _socket_connections:
.. index:: pair: connection pool; socket

Socket connection
-----------------

**musicpdaio** uses a connection pool internally to keep already opened socket
and reuse it.

When first instantiated |mpdaio.MPDClient| comes with an empty pool,
when the first MPD command is called a connection is opened, saved and
potentially reused later. In case a concurrent MPD command is called while the
connection is still in use a new connection is made and kept in the pool.

.. code-block:: python

   client = mpdaio.MPDClient()
   client.connections    # Returns an empty list: []
   client.version        # Returns empty string: ''
   await client.ping()   # A connection is made and kept open
   client.connections    # Returns a list; [Connection<example.org:6600>]

.. vim: spell spelllang=en
