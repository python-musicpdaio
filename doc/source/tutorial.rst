.. _tutorial:

Tutorials
=========

Install
-------

For now the code is in early development stage. No releases are made.

.. code:: bash

   # Use a virtualenv
   python -m venv .../path/to/my/venv
   . .../path/to/my/venv/bin/activate
   pip install git+https://codeberg.org/MusicPlayerDaemon/python-musicpdaio.git@main


.. todo::

   Updating a previous installation via `pip install git+https`.

Getting started
----------------

.. index:: single: command; searchadd

**Connect, clear the queue, add tracks**

.. sourcecode:: python

   import mpdaio

   client = mpdaio.MPDClient()
   await client.ping()         # Plain connection test
   print(client.version)       # Prints MPD's protocol version
   print(await client.clear()) # Clears the current queue
   # Add all tracks from artist "Amon Tobin"
   print(await client.searchadd('(Artist == "Amon Tobin")'))
   await client.play()
   await client.setvol(60)
   print(await client.currentsong())
   await client.close()        # Finally close connection

.. index:: single: command; password

**musicpdaio** tries to come with sane defaults, then running
:py:class:`mpdaio.MPDClient` with no explicit argument will try default values
to connect to MPD. Cf. :ref:`reference` for more about
:ref:`defaults<default_settings>`.

**Using a specific host, port and a password.**

The password is sent when a connection is made, no need to explicitly use the
password command. In the following code a client is constructed with a password argument, then when the ping method is called:

* the client fetch a connection from the pool
* then a password command is sent with the password
* finally the ping command is sent.

.. sourcecode:: python

   client = mpdaio.MPDClient(host='example.org', port='6601', password='53(237')
   await client.ping()

**Wrapping some commands in a python script**

.. literalinclude:: tutorial/tutorial-00.py

.. index:: single: command; albumart

**Fetch album art for the given track**

The logic is similar with `readpicture` command.

.. sourcecode:: python

   client = mpdaio.MPDClient()
   # Looking for cover art in 'Tool/2001-Lateralus/'
   track = 'Tool/2001-Lateralus/09-Tool - Lateralus.flac'
   aart = await cli.albumart(track, 0)
   received = int(aart.get('binary'))
   size = int(aart.get('size'))
   with open('/tmp/cover', 'wb') as cover:
       # aart = {'size': 42, 'binary': 2051, data: bytes(...)}
       cover.write(aart.get('data'))
       while received < size:
           aart = await cli.albumart(track, received)
           cover.write(aart.get('data'))
           received += int(aart.get('binary'))
       if received != size:
           print('something went wrong')
   await cli.close()

Cf. `MPD protocol documentation`_ for more binary responses.


Concurrency
-----------

.. literalinclude:: tutorial/tutorial-01.py

.. vim: spell spelllang=en
