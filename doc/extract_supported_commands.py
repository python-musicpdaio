#!/usr/bin/python3
import re
import sys

START = 'self._commands = {'
END = '}'

def find_start(fd):
    line = fd.readline()
    while START not in line:
        line = fd.readline()
        if not line:
            break
    if not line:
        print('Reach end of file!', file=sys.stderr)
        sys.exit(1)


def main():
    with open('mpdaio/client.py', 'r', encoding='utf-8') as fd:
        # fast forward to find self._commands
        find_start(fd)
        cmd_patt = '"(?P<command>.*)":'
        tit_patt = '# ?(?P<title>.*)'
        cmd_regex = re.compile(cmd_patt)
        tit_regex = re.compile(tit_patt)
        # Now extract supported commands
        line = 'foo'
        while line and END not in line:
            line = fd.readline()
            cmd = cmd_regex.search(line)
            tit = tit_regex.search(line)
            if tit:
                print(f'\n{tit[1]}')
                print('^'*len(tit[1]))
            if cmd:
                print(f'* {cmd[1]}')


if __name__ == '__main__':
    main()
