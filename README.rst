Music Player Daemon client module
*********************************

An asynchronous MPD (Music Player Daemon) client library written in Python.

----

:Documentation: https://musicplayerdaemon.codeberg.page/python-musicpdaio/
:MPD Protocol:  https://www.musicpd.org/doc/html/protocol.html
:Code:          https://codeberg.org/MusicPlayerDaemon/python-musicpdaio.git
:Dependencies:  None
:Compatibility: Python 3.11+
:Licence:       GNU LGPLv3

.. vim: spell spelllang=en
